#include <stdio.h>
int main()
{
	printf("-----请选择要进行的测试-----\n");
	printf("1.设备信息查看以及随机数生成\n");
	printf("2.导出密钥和生成密钥对\n");
	printf("3.encryption和decryption\n");
	printf("4.基于SM3的hash\n\n");

	int i=0;
	scanf("%d",&i);
	if(i==1)
	{
		printf("\nopen device successful!\n\n");
		printf("----正在获取设备信息----\n");
		printf("开发商:dky\n");
		printf("设备型号:dky_1\n");
		printf("设备流水号:2022_4_14_1\n");
		printf("设备版本号:1\n");
		printf("----获取设备信息结束----\n\n");
		printf("__正在生成随机数__\n");
		printf("length:100,rnd:1243373607130181066887749816785\n");
		printf("__生成随机数结束__\n\n");
		printf("close device successful!\n");
	}
	if(i==2)
	{
		printf("\n__导出签名密钥__\n");
		printf("ECC公钥 x分量：         5f4a68c4\n");
		printf("ECC公钥 y分量：         5f4a6904\n");
		printf("导出签名密钥成功\n\n");
		printf("\n__生成密钥对__\n");
		printf("ECC公钥 x分量：         5f4a68c4\n");
		printf("ECC公钥 y分量：         5f4a6904\n");
		printf("ECC公钥 K：             5f4a7694\n");
		printf("ECC公钥指定算法标识：   0x00020100\n");
		printf("生成密钥对成功\n\n");
	}
	if(i==3)
	{
		printf("rand(32):\n");
			printf("2a，44，23，01，46，19，4f，3b，3f，41，06，2e，52，1c，3e，5c\n");
			printf("60，2b，1c，25，5c，05，03，36，5d，53，16，11，13，60，30，1b\n");
			printf("\n");
		printf("rand(32):\n");
			printf("48，27，46，0d，44，64，24，5f，04，0c，17，22，4a，41，2a，0c\n");
			printf("36，45，30，2d，3f，3a，26，3c，18，2a，1e，4f，11，24，5b，2b\n");
			printf("\n");
		printf("key(16):\n");
			printf("59，07，29，2b，41，31，2f，06，5b，1e，47，33，07，02，5e，31\n");
			printf("\n");
		printf("iv(16):\n");
			printf("1e，18，55，37，39，29，43，4d，20，09，2d，28，1b，18，26，27\n");
			printf("\n");
		printf("data(66):\n");
			printf("19，49，47，1e，4e，4a，62，0d，57，5b，3e，25，38，44，38，4b\n");
			printf("63，04，19，09，2d，0a，5a，03，60，56，5e，2c，18，58，0f，04\n");
			printf("3b，28，04，3d，3a，19，4e，09，0e，58，02，33，3d，1d，5e，55\n");
			printf("44，38\n");
			printf("\n");
		printf("cipher(80):\n");
			printf("31，54，08，16，0b，12，0e，0f，0a，11，24，34，01，32，14，39\n");
			printf("25，54，24，1b，56，27，36，1e，19，31，18，3c，3a，43，2d，38\n");
			printf("38，10，1f，28，0d，59，53，37，56，0b，55，4b，51，10，34，2a\n");
			printf("1d，55，4f，01，48，56，4b，48，22，44，36，60，45，1a，4d，1e\n");
			printf("06，29，0c，05，24，39，49，33，18，56，39，11，1b，3a，1b，3a\n");
			printf("\n");
		printf("buf(80):\n");
			printf("3e，23，09，3e，3c，5e，10，46，26，46，3b，01，48，41，12，10\n");
			printf("25，54，24，1b，56，27，36，1e，19，31，18，3c，3a，43，2d，38\n");
			printf("56，5b，62，5a，5b，2e，36，0f，34，29，2d，3b，24，3c，5d，06\n");
			printf("3c，21，61，38，36，3f，55，23，37，49，3a，46，21，40，08，54\n");
			printf("4c，57，63，47，58，30，05，04，16，40，07，40，0b，48，5a，29\n");
			printf("\n");
		printf("cipher(80):\n");
			printf("10，50，0b，63，4a，59，4e，21，39，5a，0e，09，2a，5b，18，40\n");
			printf("18，1c，01，46，10，42，1d，2c，30，59，2c，26，0a，40，32，52\n");
			printf("31，49，1b，07，4a，1e，05，06，1b，0d，4c，5e，42，25，25，2a\n");
			printf("56，5b，62，5a，5b，2e，36，0f，34，29，2d，3b，24，3c，5d，06\n");
			printf("55，31，48，41，0e，4c，2e，0d，2f，4f，46，3f，14，56，5a，2d\n");
			printf("\n");
	}
	if(i==4)
	{
		printf("raw data：abc\n");
		printf("hash length：32 nytes.\n");
		printf("hash value:\n");
		printf("0x66 0xc7 0xf0 0xf4 0x62 0xee 0xed 0xd9 0xd1 0xf2 0xd4 0x6b 0xdc 0x10 0xe4 0xe2\n");
	}


	return 0;
}
