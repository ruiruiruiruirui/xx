#ifndef _SDF_H
#define _SDF_H
//定义设备信息结构
typedef struct DeviceInfo_st{
unsigned char IssuerName[40]; //设备生产商名称
unsigned char DeviceName[16];
unsigned char DeviceSerial[16];
unsigned int DeviceVersion;
unsigned int StandardVersion;
unsigned int AsymAlgAbility[2];
unsigned int SymAlgAbility;
unsigned int HashAlgAbility;
unsigned int BufferSize;
}DEVICEINFO;

//Error Code
#define SDF_OK 0x0 //操作成功
#define SDR_BASE   0x01000000   //错误码基础值
#define SDR_UNKNOWERR   SDR_BASE+0x00000001  //未知错误
#define SDR_NOTSUPPORT   SDR_BASE+0x00000002  //不支持的接口调用
#define SDR_COMMFAIL   SDR_BASE +0x00000003  //与设备通信失败
#define SDR_HARDFAIL    SDR_BASE+ 0x00000004   //运算模块无响应
#define SDR_OPENDEVICE   SDR_BASE+0x00000005  //打开设备失败
#define SDR_OPENSESSION  SDR_BASE + 0x00000006  //创建会话失败
#define SDR_PARDENY   SDR_BASE +0x00000007    //无私钥使用权限
#define SDR_KEYNOTEXIST   SDR_ BASE+0x00000008   //不存在的密钥调用
#define SDR_ALGNOTSUPPORT   SDR_BASE + 0x00000009  //不支持的算法调用
#define SDR_ALGMODNOTSUPPORT   SDR_BASE+ 0x0000000A   //不支持的算法模式调用
#define SDR_PKOPERR   SDR_BASE+ 0x0000000B   //公钥运算失败
#define SDR_SK OPERR  SDR_BASE + 0x0000000C  //私钥运算失败
#define SDR_SIGNERR    SDR _BASE+0x0000000D   //签名运算失败
#define SDR_VERIFYERR   SDR_BASE +0x0000000E   //验证签名失败
#define SDR_SYMOPERR   SDR_BASE+ 0x0000000F   //对称算法运算失败
#define SDR_STEPERR   SDR_BASE+0x00000010  //多步运算步骤锗误
#define SDR_FILES1ZEERR   SDR_BASE+0x00000011  //文件长度超出限制
#define SDR_FILENOEXIST   SDR_BASE+0x00000012   //指定的文件不存在
#define SDR_FILEOFSERR  SDR_BASE+0x00000013  //文件起始位置错误
#define SDR_KEYTYPEERR  SDR_BASE+0x00000014  //密钥类型缙误
#define SDR_KEYERR  SDR_BASE+0x00000015  //密钥缙误
#define SDR_ENCDATAERR  SDR_BA3E+0x00000016  //ECC加密数据错误
#define SDR_RANDERR  SDR_BASE+0x00000017  //随机数产生失败
#define SDR_PRKRERR  SDR_BASE+0x00000018  //私钥使用权限获取失败
#define SDR_MACFRR  SDR_BASE+0x00000019 //MAC运算失败
#define SDR_FILEEXISTS   SDR_BASE+ 0x0000001A  //指定文件已存在
#define SDR_FILEWERR  SDR_BASE+0x0000001B  //文件写入失败
#define SDR_NORUFFER  SDR_BASE+0x0000001c  //存储空间不足
#define SDR_INARGERR  SDR_BASE+0x0000001D  //输入参数错误
#define SDR_OUTARGERR  SDR_BASE +0x0000001E  //输出参数错误

//设备管理
/*
功能：打开密码设备
参数:
phDeviceHandle[out]返回设备句柄
返回值:
0       成功
非0   失败,返回错误代码
备注:
phDeviceHandle由函数初始化并填写内容
*/
int SDF_OpenDevice(void ** phDeviceHandle);

/*
功能：关闭密码设备,并释放相关资源。
参数:
hDeviceHandle[in]   已打开的设备句柄
返回值:
0        成功
非0     失败,返回错误代码
*/
int SDF_CloseDevice( void * hDeviceHandle);

/*
功能：获取密码设备能力描述。
参数:
hSessionHandle[in]  与设备建立的会话句柄
pstDeviceInfo [out]  设备能力描述信息,内容及格式见设备信息定义
返回值:
0        成功
非0     失败,返回错误代码 
*/
int SDF_GetDeviceInfo(
void * hSessionHandle,
DEVICEINFO * pstDeviceInfo);

/*
功能：获取指定长度的随机数。
参数：hSessionHandle[in]   与设备建立的会话句柄
uilength[in]   欲获取的随机数长度
pucRandom[out]   缓冲区指针,用于存放获取的随机数
返回值:
0        成功
非0     失败,返回错误代码 
*/
int SDF_GenerateRandom (
void * hSessionHandle,unsigned int uiLength,
unsigned char * pucRandom);


#endif
写main.c，其中调用src里定义的函数
test/main.c


#include
#include
#include "sdf.h"
int main(){
   void **pdh;
   pdh=(void **)malloc(20);  //给pdh分配空间
   int ret;
   ret = SDF_OpenDevice(pdh);  //返回handle的指针

   if(ret != SDF_OK)
   {
    printf("打开设备失败\n");
   }
   else
   {
    printf("打开设备成功！\n");
  }
   printf("查看设备信息\n");
   DEVICEINFO a;
   ret = SDF_GetDeviceInfo(*pdh,&a);
   if(ret !=SDF_OK)
           printf("查看设备信息失败！\n");
   else
           printf("查看设备信息成功！\n");
   printf("设备名字叫做%s\n",a.DeviceName);
   printf("设备版本号为%d\n",a.DeviceVersion);
   printf("想要获取的随机数长度为：\n");
   int n;
   scanf("%d",&n);
   char string[100];
   ret = SDF_GenerateRandom(*pdh,n,string);
   if(ret !=SDF_OK)
    printf("生成随机数失败！");
    else
     printf("生成的随机数为%s\n",string);
   ret = SDF_CloseDevice(*pdh);
   if(ret != SDF_OK)
   {
    printf("关闭不成功！\n");
   }
   else
   {
    printf("关闭成功！\n");
   }
}


sdf.c实现函数
src/sdf.c


#include
# include 
#include
#include "sdf.h"
#include
#include 
int SDF_OpenDevice( void ** phDeviceHandle)
{
 return SDF_OK;
}
int SDF_CloseDevice( void * hDeviceHandle)
{
 return SDF_OK;
}
int SDF_GetDeviceInfo(void * hSessionHandle,DEVICEINFO * pstDeviceInfo)
{
 DEVICEINFO di;
 strcpy(di.IssuerName,"lzySDF");
 strcpy(di.DeviceName,"SDFlzy20181309");
 strcpy(di.DeviceSerial,"20210425");
 di.DeviceVersion=1;
 (*pstDeviceInfo)= di; 
 
 return SDF_OK;
}
int SDF_GenerateRandom (void * hSessionHandle,unsigned int uiLength,unsigned char * pucRandom)
{
 BIGNUM *bn;
        
        int i;
        bn = BN_new(); //生成一个BIGNUM结构
        //int bits = 20;
        int top = -1;
        int bottom = 1;
        BN_rand(bn, uiLength, top, bottom); //生成指定bits的随机数
        char *a = BN_bn2hex(bn); //转化成16进制字符串
        puts(a);
        printf("\n");
        for(i=0;*(a+i)!='\0';i++)
        {
            *(pucRandom+i)=*(a+i);
        }
        *(pucRandom+i)='\0';
        BN_free(bn); //释放BIGNUM结构
        return SDF_OK;
}
